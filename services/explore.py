from bs4 import BeautifulSoup
from services.download import download_file
class ScrapExp:
    def scrap_category(driver):
        """
        Scrapes the page containing the different catogories listed on Moodle

        Parameters:
        driver (WebDriver): The Selenium WebDriver instance.

        Returns:
        dict: A dictionary containing category names as keys and their URLs as values.
        """
        html = driver.page_source
        psource = BeautifulSoup(html, "html.parser")
        category = psource.find_all("h3", {"class" : "categoryname"})
        dic = {}
        for cat in category:
            nom = cat.find("a").get_text(strip=True)
            url = cat.find("a").get("href")
            dic[nom] = url
        return dic

    def scrap_cours(driver):
        """
        Scrapes the page containing the different courses listed on Moodle

        Parameters:
        driver (WebDriver): The Selenium WebDriver instance.

        Returns:
        dict: A dictionary containing category names as keys and their URLs as values.
        """
        html = driver.page_source
        psource = BeautifulSoup(html, "html.parser")
        cours = psource.find_all("h3", {"class" : "coursename"})
        dic = {}
        for crs in cours:
            nom = crs.find("a").get_text(strip=True)
            url = crs.find("a").get("href")
            dic[nom] = url
        if not dic:
            cours2 = psource.find_all("div", {"class" : "coursename"})
            for crs2 in cours2:
                nom2 = crs2.find("a").get_text(strip=True)
                url2 = crs2.find("a").get("href")
                dic[nom2] = url2
        return dic

    def explore(driver):
        """
        Scrapes the page containing the different catogories listed on Moodle

        Parameters:
        driver (WebDriver): The Selenium WebDriver instance.

        Returns:
        list: A list containing 2 elements [str, dic]
                -The type of the elements cointained in the dic
                -dictionary containing category names as keys and their URLs as values. Or None.
        """
        category = ScrapExp.scrap_category(driver)
        if category:
            return ["Category", category]
        else:
            cours = ScrapExp.scrap_cours(driver)
            if cours:
                html = driver.page_source
                psource = BeautifulSoup(html, "html.parser")
                ninscrit = psource.find_all("div", {"class" : "continuebutton"})
                if ninscrit:
                    return ["Pas inscrit", None]
                return ["Cours", cours]
            else:
                return ["Télécharger", None]

    def check_cours(driver, path):
        """
        Downloads resources linked on a webpage.

        Parameters:
            driver (WebDriver): The Selenium WebDriver object.
            path (str): The directory path where downloaded files will be saved.

        Returns:
            None
        """
        html = driver.page_source
        psource = BeautifulSoup(html, "html.parser")
        topics = psource.find("ul", {"class" : "topics"})
        liens = topics.find_all("a") 
        for lien in liens:
            telecharger = lien.get("href")
            download_file(driver, telecharger)
