from bs4 import BeautifulSoup
from selenium.webdriver.remote.webdriver import WebDriver
import time

def download_file(driver: WebDriver, url: str) -> None:
    """
    Downloads files from the given URL.

    Parameters
    ----------
    driver : WebDriver
        The WebDriver instance.
    url : str
        The URL of the file to download.
    """
    if "resource" in url:
        # If the URL contains "resource", it's a direct file download link
        driver.get(url)
        url2 = driver.current_url
        dl = url2 + "?forcedownload=1"
        driver.execute_script("window.open('{}', '_blank');".format(dl))
        time.sleep(2)

    if "folder" in url:
        # If the URL contains "folder", it's a folder containing multiple files
        driver.get(url)
        html = driver.page_source
        psource = BeautifulSoup(html, "html.parser")
        sous_fichier = psource.find("div", {"class" : "ygtvchildren"})
        liens = sous_fichier.find_all("a")
        for lien in liens:
            tele = lien.get("href")
            # Open each file link in a new tab/window
            driver.execute_script("window.open('{}', '_blank');".format(tele))
            time.sleep(2)

