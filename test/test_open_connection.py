import os
import sys
import unittest
import time

# Necessary import for headless mode
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

# Add the parent directory of your module to the module search path
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

# Import your connection module
from connection import open_connection

from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

class TestAuthentication(unittest.TestCase):
    """
    Test class for authentication process.
    """

    @classmethod
    def setUpClass(cls):
        """
        Set up class method to initialize driver and authentication object.
        """
        # Initialize driver and authentication object (with headless to not open Firefox window)
        cls.options = Options()
        cls.options.add_argument('-headless')
        cls.driver = webdriver.Firefox(options=cls.options)

        cls.authentication = open_connection.Authentication(
            cas_username=os.environ["CAS_USERNAME"],
            cas_password=os.environ["CAS_PASSWORD"],
            cas_url=os.environ["CAS_URL"],
            moodle_url=os.environ["MOODLE_URL"]
        )
       
    def test_auth(self):
        """
        Test method for authentication process.
        """
        open_connection.auth(self.driver, self.authentication)

        # Wait for the page to load properly
        time.sleep(5)

        self.driver.get("https://foad-moodle.ensai.fr/course/index.php")

        # Check if the driver has navigated to the requested site
        self.assertEqual("https://foad-moodle.ensai.fr/course/index.php", self.driver.current_url)

    @classmethod
    def tearDownClass(cls):
        """
        Tear down class method to quit the driver.
        """
        cls.driver.quit()

if __name__ == "__main__":
    # Run the test suite and print the result
    result = unittest.TextTestRunner(verbosity=2).run(unittest.makeSuite(TestAuthentication))
    if result.wasSuccessful():
        print("All tests passed successfully.")
    else:
        print("At least one test failed.")