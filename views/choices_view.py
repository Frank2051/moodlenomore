from InquirerPy import prompt
from views.abstract_view import AbstractView
from services.explore import ScrapExp
import time

class ChoicesView(AbstractView):
    """
    Represents a view for making choices based on a given path and link.

    Attributes
    ----------
    path : list
        A list representing the current path in the navigation.
    link : str
        The link associated with the current view.
    nom : str
        The name associated with the current view.
    questions : list
        A list of questions to prompt the user for choices.
    """

    def __init__(self,path=[], link="",nom="") -> None:
        """
        Initializes the ChoicesView object.

        Parameters
        ----------
        path : list, optional
            A list representing the current path in the navigation, by default [].
        link : str, optional
            The link associated with the current view, by default "".
        nom : str, optional
            The name associated with the current view, by default "".
        """
        self.path=path
        self.link=link
        self.nom=nom
        self.questions = [
            {
                "type": "list",
                "name": "choix",
                "message": "Choisir à partir de : "+ nom,
                "choices": [],
            }
        ]

    def choisir_menu(self,driver):
        """
        Method to prompt the user for choices based on the current view.

        Parameters
        ----------
        driver : WebDriver
            The WebDriver instance.

        Returns
        -------
        AbstractView
            The next view based on the user's choice.
        """
        print(self.link)
        driver.get(self.link)
        resultat=ScrapExp.explore(driver)
        dic=resultat[1]
        etat =resultat[0]

        if etat =="Pas inscrit":
            self.path.pop()
            if len(self.path)==1:
                nom,lien=self.path[-1]
                from views.start_view import StartView
                print("Vous n'êtes pas inscrit à ce cours")
                time.sleep(5)
                return StartView(self.path,link=lien, nom = nom)
            nom,lien=self.path[-1]
            print("Vous n'êtes pas inscrit à ce cours")
            time.sleep(5)
            return ChoicesView(self.path,link=lien, nom = nom)           

        if etat=="Télécharger":
            from views.confirm_view import ConfirmView
            nom,lien=self.path[-1]
            return ConfirmView(self.path,link=lien, nom = nom)

        self.questions[0]["choices"]= list(dic.keys()) + ["Retour"]
        reponse = prompt(self.questions)
        
        if reponse["choix"] == "Retour":
            self.path.pop()
            if len(self.path)==1:
                nom,lien=self.path[-1]
                from views.start_view import StartView
                return StartView(self.path,link=lien, nom = nom)
            nom,lien=self.path[-1]
            return ChoicesView(self.path,link=lien, nom = nom)
        else:
            nom=reponse["choix"]
            lien=dic[nom]
            self.path.append((nom,lien))
            return ChoicesView(self.path,link=lien, nom = nom)
