"""
Vue abstraite
"""
from abc import ABC, abstractmethod
class AbstractView(ABC):
    """
    Abstract View
    """

    def __init__(self,path=[], link="", nom=""):
        """
        Initializes a new instance of the view.

        Parameters:
            path (list): List of paths taken during web scraping
            link (str): Link of the current page
            nom (str): Name of the current selection
        """         
        self.link = link
        self.nom = nom
        self.path=path

    def nettoyer_console(self):
        """
        Clears the console
        """
        print('\n' * 10)

    def afficher(self) -> None:
        """
        Print a large space in the terminal to simulate
        the application's page change
        """
        self.nettoyer_console()
        print(self.nom + " : " + self.link)
        print()

    @abstractmethod
    def choisir_menu(self):
        """
        User chooses the next menu

        Return
        ------
        view
            Returns the view chosen by the user in the terminal
        """
