from InquirerPy import prompt
from views.abstract_view import AbstractView
from views.choices_view import ChoicesView
from services.explore import ScrapExp

starter = "https://foad-moodle.ensai.fr/course/index.php"
starter_name="MesCours"

class StartView(AbstractView):
    """
    Represents the starting view for navigating courses.

    Attributes
    ----------
    path : list
        A list representing the current path in the navigation.
    link : str
        The link associated with the current view.
    nom : str
        The name associated with the current view.
    questions : list
        A list of questions to prompt the user for choices.
    """

    def __init__(self,path=[(starter_name,starter)],link=starter,nom=starter_name) -> None:
        """
        Initializes the StartView object.

        Parameters
        ----------
        path : list, optional
            A list representing the current path in the navigation, by default [(starter_name, starter)].
        link : str, optional
            The link associated with the current view, by default starter.
        nom : str, optional
            The name associated with the current view, by default starter_name.
        """
        self.link = link
        self.nom = nom
        self.path=path
        self.questions = [
            {
                "type": "list",
                "name": "choix",
                "message": "Choisir à partir de : "+ nom,
                "choices": [],
            }
        ]

    def choisir_menu(self,driver):
        """
        Method to prompt the user for choices based on the current view.

        Parameters
        ----------
        driver : WebDriver
            The WebDriver instance.

        Returns
        -------
        AbstractView or None
            The next view based on the user's choice, or None if the user chooses to quit.
        """
        self.path=[(starter_name,starter)]
        self.link=starter
        driver.get(self.link)
             
        resultat=ScrapExp.explore(driver)
        dic=resultat[1]
        self.questions[0]["choices"]= list(dic.keys()) + ["Quitter"]
        reponse = prompt(self.questions)

        if reponse["choix"] == "Quitter":
            return None
        else:
            nom=reponse["choix"]
            lien=dic[nom]
            self.path.append((nom,lien))
            return ChoicesView(self.path,link=lien, nom = nom)
        