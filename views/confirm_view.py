from InquirerPy import prompt
from views.abstract_view import AbstractView
from views.choices_view import ChoicesView
from views.start_view import StartView
from services.explore import ScrapExp

class ConfirmView(AbstractView):
    """
    Represents a view for confirming actions.

    Attributes
    ----------
    path : list
        A list representing the current path in the navigation.
    link : str
        The link associated with the current view.
    nom : str
        The name associated with the current view.
    questions : list
        A list of questions to prompt the user for confirmation.
    """
 
    def __init__(self,path=[], link="",nom="") -> None:
        """
        Initializes the ConfirmView object.

        Parameters
        ----------
        path : list, optional
            A list representing the current path in the navigation, by default [].
        link : str, optional
            The link associated with the current view, by default "".
        nom : str, optional
            The name associated with the current view, by default "".
        """
        self.path=path
        self.link=link
        self.nom=nom
        self.questions = [
            {
                "type": "list",
                "name": "choix",
                "message": "Voulez-vous télécharger : "+ nom,
                "choices": ["oui","non"],
            }
        ]

    def choisir_menu(self,driver):
        """
        Method to prompt the user for confirmation.

        Parameters
        ----------
        driver : WebDriver
            The WebDriver instance.

        Returns
        -------
        AbstractView
            The next view based on the user's choice.
        """
        reponse = prompt(self.questions)
        
        if reponse["choix"] == "non":
            self.path.pop()
            nom,lien=self.path[-1]
            return ChoicesView(self.path,link=lien, nom = nom)

        if reponse["choix"] == "oui":
            ScrapExp.check_cours(driver,path=self.path)
            return StartView()
