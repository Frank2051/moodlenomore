# moodlenomore

# Get started

Download a firefox driver, add it to `PATH`.

Install the project : `pip install requirements`

Create a .env with a real username and password using the exemple_env as template

run the main `python moodlenomore/main.py`
(Si vous rencontrer un problème fermez les onglets visuels/lecteur de téléchargement à la main sauf le premier onglet actif)

enjoy 😊

# Launch test

run `python test/test_open_connection.py` 

# Objectif

L'objectif de ce code est de permettre la navigation dans Moodle afin de télécharger les contenus des cours.

# Organisation

Le projet est organisé en quatre modules :
- Un module de connexion qui gère l'authentification.
- Un module de service qui se charge du scrapping et du téléchargement.
- Un module de vue qui gère les interfaces utilisateur.
- Un module de test permettant de tester l'authentification.

Le code comprend également un fichier principal (main), un fichier .gitignore, un fichier README, un fichier requirements, un exemple_env (qui fournit un modèle pour créer un fichier .env), et un fichier yml (les tests n'ont pas pu être exécutés en dehors de l'environnement local).

# Fonctionnement

Le fonctionnement du code est le suivant :
1. Création d'un driver pour s'authentifier sur la page de connexion de Moodle.
2. Instanciation d'une vue qui appelle le driver pour accéder à la page d'accueil de Moodle.
3. Utilisation d'un scraper pour identifier les liens des catégories de cours sur la page d'accueil.
4. Utilisation de InquirerPy pour permettre à l'utilisateur de choisir la catégorie souhaitée dans le terminal.
5. Répétition du processus jusqu'à atteindre un cours spécifique. (L'utilisateur a également la possibilité de revenir en arrière en cas d'erreur.)
6. Demande de confirmation à l'utilisateur avant de télécharger la page.
7. Téléchargement de la page.
8. Redirection de l'utilisateur vers le choix de catégorie au niveau de la page d'accueil. (L'utilisateur a également la possibilité d'arrêter le programme.)
