from connection.open_connection import Authentication, get_web_driver, auth
import time
from views.start_view import StartView

if __name__ == "__main__":
    from dotenv import load_dotenv

    load_dotenv()

    import os

    cas_username = os.environ.get("CAS_USERNAME")
    cas_password = os.environ.get("CAS_PASSWORD")
    cas_url = os.environ.get("CAS_URL")
    moodle_url = os.environ.get("MOODLE_URL")
    driver = get_web_driver()
    auth(
        driver=driver,
        authentication=Authentication(
            cas_username=cas_username,
            cas_password=cas_password,
            cas_url=cas_url,
            moodle_url=moodle_url,
        ),
    )
    # Le temps que la page charge bien
    time.sleep(5)

    # Connexion au sommaire de moodle
    # driver.get("https://foad-moodle.ensai.fr/course/index.php")
    
    vue_courante=StartView()
    NB_ERREURS = 0

    while vue_courante:
        if NB_ERREURS > 100:
            print("Félicitations, vous avez atteint 100 erreurs!")
            break
        try:
            # Affichage du menu
            vue_courante.afficher()

            # Affichage des choix possibles
            vue_courante = vue_courante.choisir_menu(driver)
        except Exception as e:
            print(e)
            NB_ERREURS += 1
            vue_courante = StartView(
                nom="Retour aux sources"
            )

    # Pour fermer la connection
    driver.close()
