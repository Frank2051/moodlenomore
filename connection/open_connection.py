from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

class Authentication:
    def __init__(
        self, cas_username: str, cas_password: str, cas_url: str, moodle_url: str) -> None:
        """
        Initializes the Authentication object with CAS and Moodle URLs, along with the CAS credentials.

        Parameters
        ----------
        cas_username : str
            The CAS username.
        cas_password : str
            The CAS password.
        cas_url : str
            The URL of the CAS server.
        moodle_url : str
            The URL of the Moodle server.
        """
        self.__cas_username = cas_username
        self.__cas_password = cas_password
        self.__cas_url = cas_url
        self.__moodle_url = moodle_url

    def get_auth_url(self) -> str:
        """
        Generates the CAS authentication URL with the Moodle service.

        Returns
        -------
        str
            The CAS authentication URL with the Moodle service.
        """
        return f"{self.__cas_url}?service={self.__moodle_url}"

    @property
    def cas_username(self) -> str:
        """
        Returns the CAS username.

        Returns
        -------
        str
            The CAS username.
        """
        return self.__cas_username

    @property
    def cas_password(self) -> str:
        """
        Returns the CAS password.

        Returns
        -------
        str
            The CAS password.
        """
        return self.__cas_password

def get_web_driver() -> webdriver.Firefox:
    """
    Instantiates a Firefox WebDriver.

    Returns
    -------
    webdriver.Firefox
        An instance of the Firefox WebDriver.
    """
    driver = webdriver.Firefox()
    return driver

def auth(driver: webdriver.Firefox, authentication: Authentication) -> None:
    """
    Authenticates the user using CAS authentication.

    Parameters
    ----------
    driver : webdriver.Firefox
        The WebDriver instance.
    authentication : Authentication
        The Authentication object containing CAS credentials and URLs.
    """
    driver.get(authentication.get_auth_url())
    assert "CAS" in driver.title
    elem_username = driver.find_element(By.ID, "username")
    elem_username.send_keys(authentication.cas_username)
    elem_username.send_keys(Keys.RETURN)
    elem_password = driver.find_element(By.ID, "password")
    elem_password.send_keys(authentication.cas_password)
    elem_password.send_keys(Keys.RETURN)
